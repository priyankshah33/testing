import { Component } from '@angular/core';

@Component({
    templateUrl:'./404.component.html',
    styles: [`
        .errorMessage { 
            margin-top: 150px;
            font-size: 120px;
            text-alignL center;
        }
    `]
})

export class Error404Component {
}
