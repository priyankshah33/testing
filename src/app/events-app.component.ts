import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'events-app',
  template: `
  <nav-bar *ngIf="router.url != '/404'"></nav-bar>
  <router-outlet></router-outlet>
  `
})
export class EventsAppComponent {
  title = 'angular-events';

  constructor(private router: Router ){

  }
  

}

